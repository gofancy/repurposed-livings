import com.matthewprenger.cursegradle.CurseArtifact
import com.matthewprenger.cursegradle.CurseProject
import com.matthewprenger.cursegradle.CurseRelation
import java.time.LocalDateTime

plugins {
    id("net.minecraftforge.gradle") version "5.1.+"
    id("org.parchmentmc.librarian.forgegradle") version "1.+"
    id("wtf.gofancy.convention.publishing")
    id("org.ajoberstar.reckon") version "0.16.1"
    id("com.matthewprenger.cursegradle") version "1.4.+"
    id("com.modrinth.minotaur") version "2.+"
}

group = "wtf.gofancy.mc"

val versionMc: String by project
val curseForgeId: String by project
val modrinthId: String by project

val publishVersionName = provider { "Repurposed Livings $version" }
val publishReleaseType = System.getenv("PUBLISH_RELEASE_TYPE") ?: "release"

reckon {
//    stages("beta", "rc", "final") for now let's use snapshots
    snapshots()

    // only needed when using Reckon's tasks, used to calculate the name of the tag to create
    setStageCalc(calcStageFromProp())
    setScopeCalc(calcScopeFromProp())
    
    setDefaultInferredScope("patch")
}

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(17))
}

minecraft {
    mappings("parchment", "2022.08.14-1.19.2")
    
    accessTransformer(file("src/main/resources/META-INF/accesstransformer.cfg"))
    
    runs {
        create("client") {
            workingDirectory = file("run").canonicalPath

            properties(mapOf(
                "forge.logging.markers" to "REGISTRIES",
                "forge.logging.console.level" to "debug",
                "forge.enabledGameTestNamespaces" to "repurposed-livings"
            ))
        }
        
        create("server") {
            workingDirectory = file("run").canonicalPath
            
            properties(mapOf(
                "forge.logging.markers" to "REGISTRIES",
                "forge.logging.console.level" to "debug",
                "forge.enabledGameTestNamespaces" to "repurposed-livings"
            ))
        }
        
        create("gameTestServer") {
            workingDirectory = file("run").canonicalPath
                    
            properties(mapOf(
                "forge.logging.markers" to "REGISTRIES",
                "forge.logging.console.level" to "debug",
                "forge.enabledGameTestNamespaces" to "repurposed-livings"
            ))
        }

        create("data") {
            workingDirectory = file("run").canonicalPath
            
            properties(mapOf(
                "forge.logging.markers" to "REGISTRIES",
                "forge.logging.console.level" to "debug"
            ))
            
            args("--mod", "repurposedlivings", "--all", "--output", file("src/generated/resources/"), "--existing", file("src/main/resources/"))
        }
    }
}

sourceSets.main { resources.srcDir("src/generated/resources") }

repositories {
    maven {
        name = "Cursemaven"
        url = uri("https://cursemaven.com")
    }
}

dependencies {
    minecraft(group = "net.minecraftforge", name = "forge", version = "1.19.2-43.1.1")
    
    compileOnly(fg.deobf("curse.maven:the-one-probe-245211:3871444"))
}

// Example for how to get properties into the manifest for reading at runtime.
tasks {
    jar {
        finalizedBy("reobfJar")
        manifest.attributes(
            "Specification-Title" to project.name,
            "Specification-Vendor" to "Su5ed, Nikx, Silk",
            "Specification-Version" to "1",
            "Implementation-Title" to project.name,
            "Implementation-Version" to archiveVersion.get(),
            "Implementation-Vendor" to "Su5ed, Nikx, Silk",
            "Implementation-Timestamp" to LocalDateTime.now()
        )
    }
    
    withType<JavaCompile> {
        options.encoding = "UTF-8"
    }
}

curseforge {
    apiKey = System.getenv("CURSEFORGE_TOKEN") ?: "UNKNOWN"
    project(closureOf<CurseProject> {
        id = curseForgeId
        changelogType = "markdown"
//        changelog = System.getenv("CHANGELOG") ?: ""
        releaseType = publishReleaseType
        mainArtifact(tasks.jar.get(), closureOf<CurseArtifact> {
            displayName = publishVersionName.get()
            relations(closureOf<CurseRelation> {
                optionalDependency("the-one-probe")
            })
        })
        addGameVersion("Forge")
        addGameVersion(versionMc)
    })
}

modrinth {
    token.set(System.getenv("MODRINTH_TOKEN"))
    projectId.set(modrinthId)
    versionName.set(publishVersionName)
    versionType.set(publishReleaseType)
    uploadFile.set(tasks.jar.get())
    gameVersions.addAll(versionMc)
    // TODO changelog
}

publishing {
    // the publishing convention plugin will automatically either add the snapshot or the releases repo
    publications {
        register<MavenPublication>("maven") {
            from(project.components.getByName("java"))
        }
    }
}

